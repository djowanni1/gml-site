Active: yes
Title: Home
Category: home
Id: home

Graphics and Media Lab was established in 1998 as a part of [CMC Faculty](https://cs.msu.ru/en) of
[Lomonosov Moscow State University](https://www.msu.ru/en/).

Laboratory possesses solid research
experience in many areas of computer graphics, computer vision and video
processing. Research is supported by Russian fund for basic research, state
contracts, and by international companies Huawei, Intel, Microsoft, Samsung.

The head of laboratory is Dr. Dmitriy Vatolin.
