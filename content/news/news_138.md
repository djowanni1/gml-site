Active: yes
Title: Interview of Anton Konushin for Postnauka.ru
Category: news
Date: 2017-10-06 00:00

[Head of Graphics&Media Lab, CMC MSU, Anton Konushin gave interview to PostNauka.ru about technical limitations of virtual reality.](https://postnauka.ru/talks/80974)
