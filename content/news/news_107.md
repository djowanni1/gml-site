Active: yes
Title: Congratulations to Olga Senyukova and Mikhail Erofeev who have won the Russian President scholarship
Category: news
Date: 2015-03-18 00:00

Congratulations to [Olga Senyukova]({filename}/people/olga_senyukova.md) and [Mikhail Erofeev]({filename}/people/mikhail_erofeev.md) who have won the contest on receiving [the Russian President scholarship for young scientists and PhD students](https://grants.extech.ru/grants/res/winners.php?OZ=5&TZ=U&year=2015).
