Active: yes
Title: Presidential grant for young scientists
Category: news
Date: 2016-12-05 00:00

[Olga Senyukova]({filename}/people/olga_senyukova.md) has won the Grant of President of Russian Federation for young scientists with her project "Research and development of algorithms for automatic analysis of cardiological data". ([List of winners in computer science for 2017](https://grants.extech.ru/grants/res/winners.php?OZ=9&TZ=K&year=2017)). We congratulate Olga with this significant achievement!
