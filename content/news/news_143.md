Active: yes
Title: Olga Senyukova won Presidential grant
Category: news
Date: 2017-10-26 00:00

Graduate of our lab, Olga Senyukova, who won Presidential grant for state support of young scientists, received a certificate in the Central House of Scientists of the Russian Academy of Sciences. Сongratulations to Olga!

On October 24, the Central House of Scientists of the Russian Academy of Sciences held a solemn presentation of certificates to the winners of the 2017 contest to receive grants from the President of the Russian Federation for state support of young Russian scientists - Ph.D. and doctors of science.

We are proud to report that our graduate, Olga Senyukova was one of the winners.

The ceremony was attended by President of the Russian Academy of Sciences A. M. Sergeev, Head of the Federal Agency of Scientific Organizations M. M. Kotyukov, Deputy Minister of Education and Science G. V. Trubnikov.
