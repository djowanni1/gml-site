pelican==4.6.0
markdown==3.3.4
typogrify==2.0.7
bibtexparser==1.2.0
pelican-render-math==1.0.3
beautifulsoup4==4.9.3
selenium==3.141.0
transliterate==1.10.2
